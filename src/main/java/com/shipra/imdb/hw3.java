package com.shipra.imdb;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JTextArea;

public class hw3 extends JFrame {
    Connection conn;
    SharedData sharedData;

    private JPanel contentPane;
    private JTextField fromTextField;
    private JTextField toTextField;
    private JPanel countryPanel;
    private JPanel actorPanel;
    private JPanel dirPanel;
    private JPanel tagIdPanel;
    private JPanel tagWeightPanel;
    private JTextField textField;
    private JTextArea queryDisplay;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        final SharedData sharedData = new SharedData();
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    hw3 frame = new hw3(sharedData);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void createDbConn() throws SQLException {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return;
        }

        String host = "localhost";
        String dbName = "XE";
        int port = 49161;
        String oracleURL = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;

        String username = "scott";
        String password = "tiger";
        conn = DriverManager.getConnection(oracleURL, username, password);
        System.out.println("Conn success");

    }

    /**
     * Create the frame.
     * 
     * @throws SQLException
     */
    public hw3(SharedData sharedData) throws SQLException {
        this.sharedData = sharedData;
        createDbConn();
        setBackground(new Color(245, 222, 179));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1200, 750);
        contentPane = new JPanel();
        contentPane.setForeground(new Color(192, 192, 192));
        contentPane.setBackground(new Color(0, 0, 128));
        contentPane.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel movieAttrLabel = new JLabel("Movies Attributes");
        movieAttrLabel.setBounds(0, 0, 894, 33);
        movieAttrLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
        movieAttrLabel.setForeground(new Color(0, 0, 0));
        movieAttrLabel.setOpaque(true);
        movieAttrLabel.setBackground(new Color(135, 206, 250));
        movieAttrLabel.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(movieAttrLabel);

        // genre column
        // --------------------------------------------------------------------
        JPanel genrePanel = new JPanel();
        genrePanel.setBackground(Color.WHITE);

        JScrollPane genreScrollPane = new JScrollPane(genrePanel);
        genreScrollPane.setBounds(0, 32, 244, 272);
        genreScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        genreScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        contentPane.add(genreScrollPane);

        JLabel genreLabel = new JLabel("Movie Genre");
        genreLabel.setBackground(Color.LIGHT_GRAY);
        genreScrollPane.setColumnHeaderView(genreLabel);
        genreLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        genreLabel.setHorizontalAlignment(SwingConstants.CENTER);
        genreLabel.setOpaque(true);

        genrePanel.setLayout(new GridLayout(20, 1, 0, 1));

        String loadGenre = "SELECT DISTINCT(GENRE) FROM MOVIE_GENRE";
        Statement loadGenreSt = conn.createStatement();
        loadGenreSt.execute(loadGenre);
        ResultSet rsLoadGenre = loadGenreSt.getResultSet();
        List<JCheckBox> genreCheckBox = new ArrayList<JCheckBox>();
        int count = 0;
        if (rsLoadGenre != null) {
            while (rsLoadGenre.next()) {
                JCheckBox checkBox = new JCheckBox(rsLoadGenre.getString(1));
                // checkBox.setBounds(19, 47, 207, 23);
                genreCheckBox.add(count, checkBox);
                count++;
            }
        }

        for (int i = 0; i < genreCheckBox.size(); i++) {
            genrePanel.add(genreCheckBox.get(i));
            genreCheckBox.get(i).addItemListener(new GenreItemListener(sharedData, this));
        }

        // Genre column
        // -----------------------------------------------------------------------------------------------------

        // Year column
        // -------------------------------------------------------------------------

        JPanel yearPanel = new JPanel();
        yearPanel.setBounds(0, 303, 245, 117);
        yearPanel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(128, 128, 128)));
        contentPane.add(yearPanel);
        yearPanel.setLayout(null);

        JLabel lblMovieYear = new JLabel("Movie Year\n");
        lblMovieYear.setBackground(new Color(192, 192, 192));
        lblMovieYear.setOpaque(true);
        lblMovieYear.setHorizontalAlignment(SwingConstants.CENTER);
        lblMovieYear.setBounds(0, 0, 245, 30);
        yearPanel.add(lblMovieYear);

        JLabel lblFrom = new JLabel("From");
        lblFrom.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblFrom.setHorizontalAlignment(SwingConstants.CENTER);
        lblFrom.setBounds(10, 42, 47, 30);
        yearPanel.add(lblFrom);

        JLabel lblTo = new JLabel("To");
        lblTo.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblTo.setHorizontalAlignment(SwingConstants.CENTER);
        lblTo.setBounds(0, 84, 57, 16);
        yearPanel.add(lblTo);

        fromTextField = new JTextField();
        fromTextField.setHorizontalAlignment(SwingConstants.CENTER);
        fromTextField.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        fromTextField.setBounds(61, 43, 79, 28);
        yearPanel.add(fromTextField);
        fromTextField.setColumns(10);
        // fromTextField.addActionListener(new
        // FromYearActionListener(sharedData, this));

        toTextField = new JTextField();
        toTextField.setHorizontalAlignment(SwingConstants.CENTER);
        toTextField.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        toTextField.setBounds(61, 78, 79, 28);
        yearPanel.add(toTextField);
        toTextField.setColumns(10);
        
                JButton goBtn = new JButton("GO");
                goBtn.setBounds(152, 57, 75, 30);
                yearPanel.add(goBtn);
                goBtn.setForeground(new Color(255, 255, 255));
                goBtn.setFont(new Font("Lao Sangam MN", Font.BOLD, 17));
                goBtn.setBackground(new Color(0, 0, 0));
                goBtn.setOpaque(true);
                goBtn.setBorderPainted(false);
                goBtn.addActionListener(new GoActionListener(sharedData, this));

        // toTextField.addActionListener(new ToYearActionListener(sharedData,
        // this));

        // year column
        // -------------------------------------------------------------------------------------

        // first query generation

        // Countries column
        // ------------------------------------------------------------------------------------------------
        countryPanel = new JPanel();
        countryPanel.setBackground(Color.WHITE);
        // countryPanel.setVisible(true);
        JScrollPane countryScrollPane = new JScrollPane(countryPanel);
        countryScrollPane.setBounds(244, 32, 190, 388);
        countryPanel.setLayout(new GridLayout(50, 1, 0, 1));

        countryScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        countryScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        countryScrollPane.setViewportBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
        contentPane.add(countryScrollPane);

        JLabel lblCountries = new JLabel("Countries");
        lblCountries.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblCountries.setOpaque(true);
        lblCountries.setHorizontalAlignment(SwingConstants.CENTER);
        countryScrollPane.setColumnHeaderView(lblCountries);

        // -----------------------------------
        JScrollPane tscrollPane = new JScrollPane();
        tscrollPane.setBounds(663, 32, 231, 388);
        tscrollPane.setViewportBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
        contentPane.add(tscrollPane);

        JLabel lblTags = new JLabel("Tags IDs and Values");
        lblTags.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblTags.setHorizontalAlignment(SwingConstants.CENTER);
        lblTags.setOpaque(true);
        tscrollPane.setColumnHeaderView(lblTags);

        JPanel tagPanel = new JPanel();
        tagPanel.setBackground(Color.WHITE);
        tscrollPane.setViewportView(tagPanel);
        tagPanel.setLayout(null);

        tagWeightPanel = new JPanel();
        tagWeightPanel.setBounds(0, 256, 225, 107);
        tagPanel.add(tagWeightPanel);
        tagWeightPanel.setBackground(Color.WHITE);
        tagWeightPanel.setLayout(null);
        
        
        

        JScrollPane tagsScrollPane = new JScrollPane();
        tagsScrollPane.setBounds(0, 0, 225, 258);
        tagPanel.add(tagsScrollPane);

        tagIdPanel = new JPanel();

        tagsScrollPane.setViewportView(tagIdPanel);
        tagsScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        tagsScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        tagIdPanel.setBackground(Color.WHITE);
        tagIdPanel.setLayout(new GridLayout(50, 1, 0, 1));

        JScrollPane castScrollPane = new JScrollPane();
        castScrollPane.setBounds(434, 50, 231, 370);
        castScrollPane.setViewportBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
        contentPane.add(castScrollPane);

        JLabel lblActorName = new JLabel("Actor/Actress");
        castScrollPane.setColumnHeaderView(lblActorName);
        lblActorName.setVerticalAlignment(SwingConstants.TOP);
        lblActorName.setHorizontalAlignment(SwingConstants.CENTER);

        JPanel actorDirPanel = new JPanel();
        actorDirPanel.setBackground(Color.WHITE);
        castScrollPane.setViewportView(actorDirPanel);
        actorDirPanel.setLayout(null);

        actorPanel = new JPanel();
        actorPanel.setBackground(Color.WHITE);
        actorPanel.setBounds(0, 0, 225, 211);
        actorDirPanel.add(actorPanel);
        actorPanel.setLayout(null);

        JLabel lblDirector = new JLabel("Director");
        lblDirector.setOpaque(true);
        lblDirector.setBackground(Color.LIGHT_GRAY);
        lblDirector.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblDirector.setHorizontalAlignment(SwingConstants.CENTER);
        lblDirector.setBounds(0, 211, 225, 19);
        actorDirPanel.add(lblDirector);

        dirPanel = new JPanel();
        dirPanel.setBackground(Color.WHITE);
        dirPanel.setBounds(0, 231, 225, 117);
        actorDirPanel.add(dirPanel);
        dirPanel.setLayout(null);

        JScrollPane scrollPane_4 = new JScrollPane();
        scrollPane_4.setBounds(896, 32, 304, 388);
        scrollPane_4.setViewportBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
        contentPane.add(scrollPane_4);

        JLabel lblMovieResults = new JLabel("Movie Results");
        lblMovieResults.setBounds(896, 0, 304, 33);
        lblMovieResults.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
        lblMovieResults.setBackground(new Color(135, 206, 250));
        lblMovieResults.setOpaque(true);
        lblMovieResults.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblMovieResults);

        JLabel lblCast = new JLabel("CAST");
        lblCast.setBounds(434, 34, 231, 16);
        lblCast.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        contentPane.add(lblCast);
        lblCast.setBackground(Color.LIGHT_GRAY);
        lblCast.setOpaque(true);
        lblCast.setHorizontalAlignment(SwingConstants.CENTER);

        // AndOrPanel
        JPanel panel_AndOr = new JPanel();
        panel_AndOr.setBounds(0, 422, 894, 50);
        panel_AndOr.setBackground(new Color(135, 206, 250));
        panel_AndOr.setOpaque(true);
        contentPane.add(panel_AndOr);
        panel_AndOr.setLayout(null);

        JLabel lblNewLabel = new JLabel("Search Between Attributes' Values : ");
        lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblNewLabel.setBounds(48, 8, 259, 36);
        panel_AndOr.add(lblNewLabel);

        JComboBox comboBox = new JComboBox();
        comboBox.setBounds(343, 15, 116, 27);
        comboBox.setModel(new DefaultComboBoxModel(new String[] { "AND", "OR" }));
        comboBox.setToolTipText("AND or OR\n");
        comboBox.addActionListener(new AndOrActionListener(sharedData, this));
        panel_AndOr.add(comboBox);

        JButton btnMovieQuery = new JButton("Execute Movie Query");
        btnMovieQuery.setBounds(130, 646, 202, 44);
        btnMovieQuery.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        btnMovieQuery.setBackground(Color.LIGHT_GRAY);
        btnMovieQuery.setOpaque(true);
        btnMovieQuery.setBorderPainted(false);
        btnMovieQuery.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String finalQuery = generateFinalQuery();
                queryDisplay.setVisible(true);
                queryDisplay.setText(finalQuery);
            }
            
        });
        contentPane.add(btnMovieQuery);

        JButton btnUserQuery = new JButton("Execute User Query");
        btnUserQuery.setBounds(463, 646, 202, 44);
        btnUserQuery.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        btnUserQuery.setBackground(Color.LIGHT_GRAY);
        btnUserQuery.setOpaque(true);
        btnUserQuery.setBorderPainted(false);
        contentPane.add(btnUserQuery);

        JScrollPane scrollPane_6 = new JScrollPane();
        scrollPane_6.setBounds(896, 422, 304, 285);
        scrollPane_6.setViewportBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(192, 192, 192)));
        contentPane.add(scrollPane_6);

        JLabel lblUserResults = new JLabel("User Results");
        lblUserResults.setBackground(new Color(135, 206, 250));
        lblUserResults.setOpaque(true);
        lblUserResults.setFont(new Font("Lucida Grande", Font.PLAIN, 18));
        lblUserResults.setHorizontalAlignment(SwingConstants.CENTER);
        scrollPane_6.setColumnHeaderView(lblUserResults);
        
        JPanel finalQpanel = new JPanel();
        finalQpanel.setBackground(Color.WHITE);
        finalQpanel.setBounds(28, 486, 731, 129);
        contentPane.add(finalQpanel);
        finalQpanel.setLayout(null);
        
        queryDisplay = new JTextArea();
        queryDisplay.setBackground(Color.WHITE);
        queryDisplay.setBounds(6, 5, 705, 118);
        finalQpanel.add(queryDisplay);

    }

    private void resetList() throws SQLException {
        sharedData.countries = new HashSet<>();
        sharedData.actors = new HashSet<>();
        sharedData.directors = new HashSet<>();
        sharedData.tagList = new HashSet<>();

        contentPane.revalidate();
        contentPane.repaint();
    }

    private void resetSelected() {
        sharedData.selectedCountry = new HashSet<>();
        sharedData.selectedActors = new HashSet<>();
        sharedData.selectedDirector = null;
        sharedData.selectedTags = new HashSet<>();
    }

    String movieIdFromSelectedCountries() {
        if (sharedData.selectedCountry.isEmpty()) {
            return null;
        }

        String countryList = "";
        Iterator<String> itr = sharedData.selectedCountry.iterator();
        int count = 0;
        while (itr.hasNext() && count < sharedData.selectedCountry.size() - 1) {
            countryList = countryList + "'" + itr.next() + "', ";
            count++;
        }
        countryList = countryList + "'" + itr.next() + "'";
        String mids = "SELECT MOVIEID FROM MOVIE_COUNTRIES WHERE COUNTRY = ANY ( " + countryList + " )";
        return mids;
    }

    String movieIdFromSelectedActors() {
        if (sharedData.selectedActors.isEmpty()) {
            return null;
        }

        String actorList = "";
        Iterator<String> itr = sharedData.selectedActors.iterator();
        int count = 0;
        while (itr.hasNext() && count < sharedData.selectedActors.size() - 1) {
            actorList = actorList + "'" + itr.next() + "', ";
            count++;
        }
        actorList = actorList + "'" + itr.next() + "'";
        String mids = "SELECT MOVIEID FROM MOVIE_ACTORS WHERE ACTORNAME = ANY ( " + actorList + " )";
        return mids;
    }

    String movieIdFromSelectedDirectors() {
        if (sharedData.selectedDirector == null) {
            return null;
        }

        String mids = "SELECT MOVIEID FROM MOVIE_DIRECTORS WHERE DIRECTORNAME = '" + sharedData.selectedDirector + "'";
        return mids;
    }

    void queryCountryFromDB() throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT COUNTRY FROM MOVIE_COUNTRIES WHERE MOVIEID IN (");
        sb.append(generateGenreYearQuery());
        sb.append(")");
        System.out.println(sb.toString());
        try (Statement displayCountrySt = conn.createStatement();
                ResultSet displayCountryStRS = displayCountrySt.executeQuery(sb.toString());) {
            if (displayCountryStRS != null) {
                Set<String> countryList = new HashSet<>();
                while (displayCountryStRS.next()) {
                    countryList.add(displayCountryStRS.getString(1));
                }
                sharedData.countries = countryList;
            }
        }
    }

    void queryActorFromDB() throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT ACTORNAME FROM MOVIE_ACTORS WHERE MOVIEID IN (");
        sb.append(generateGenreYearQuery());
        String mid = movieIdFromSelectedCountries();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        sb.append(")");
        System.out.println(sb.toString());
        try (Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(sb.toString());) {
            if (resultSet != null) {
                Set<String> actors = new HashSet<>();
                while (resultSet.next()) {
                    actors.add(resultSet.getString(1));
                }
                sharedData.actors = actors;
            }
        }
    }

    void queryDirectorFromDB() throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT DISTINCT DIRECTORNAME FROM MOVIE_DIRECTORS WHERE MOVIEID IN (");
        sb.append(generateGenreYearQuery());
        String mid = movieIdFromSelectedCountries();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        mid = movieIdFromSelectedActors();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        sb.append(")");
        System.out.println(sb.toString());
        try (Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(sb.toString());) {
            if (resultSet != null) {
                Set<String> directors = new HashSet<>();
                while (resultSet.next()) {
                    directors.add(resultSet.getString(1));
                }
                sharedData.directors = directors;
            }
        }

    }

    void queryTagsFromDB() throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT MT.TAGID, T.VALUE FROM TAGS T, MOVIE_TAGS MT WHERE T.ID = MT.TAGID AND MOVIEID IN (");
        sb.append(generateGenreYearQuery());
        String mid = movieIdFromSelectedCountries();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        mid = movieIdFromSelectedActors();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        mid = movieIdFromSelectedDirectors();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        sb.append(")");
        System.out.println(sb.toString());
        try (Statement statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(sb.toString());) {
            if (resultSet != null) {
                Set<String> tags = new HashSet<>();
                while (resultSet.next()) {
                    tags.add(resultSet.getString(1) + "\t" + resultSet.getString(2));
                }
                sharedData.tagList = tags;
            }
        }
    }
    
    

    private String generateGenreYearQuery() {
        String genreYearQuery = null;
        String genreList = "";
        Iterator<String> itrGenre = sharedData.selectedGenre.iterator();
        int count = 0;
        while (itrGenre.hasNext() && count < sharedData.selectedGenre.size() - 1) {
            genreList = genreList + "'" + itrGenre.next() + "', ";
            count++;
        }
        genreList = genreList + "'" + itrGenre.next() + "'";
        if (sharedData.getAndOrChoice().equals("OR")) {
            genreYearQuery = "SELECT M.MOVIEID FROM MOVIES_DATA M, MOVIE_GENRE MG "
                    + "WHERE M.MOVIEID = MG.MOVIEID AND " + "M.YEAR BETWEEN " + sharedData.getFromYear() + " AND "
                    + sharedData.getToYear() + " AND MG.GENRE = ANY (" + genreList + ")";
        } else if (sharedData.getAndOrChoice().equals("AND")) {
            genreYearQuery = "SELECT M.MOVIEID FROM MOVIES_DATA M, MOVIE_GENRE MG "
                    + "WHERE M.MOVIEID = MG.MOVIEID AND " + "M.YEAR BETWEEN " + sharedData.getFromYear() + " AND "
                    + sharedData.getToYear() + " AND MG.GENRE = ALL (" + genreList + ")";
        }

        return genreYearQuery;
    }

    class GenreItemListener implements ItemListener {
        SharedData sharedData;
        hw3 hw3;

        public GenreItemListener(SharedData sharedData, final hw3 hw3) {
            this.sharedData = sharedData;
            this.hw3 = hw3;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            System.out.println("Inside actionListener genre.");
            JCheckBox cBox = (JCheckBox) e.getItem();
            String genre = cBox.getText();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                sharedData.selectedGenre.add(genre);
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                if (sharedData.selectedGenre.contains(genre)) {
                    sharedData.selectedGenre.remove(genre);
                }
            }
        }
    }

    class AndOrActionListener implements ActionListener {
        SharedData sharedData;
        hw3 hw3;

        public AndOrActionListener(SharedData sharedData, final hw3 hw3) {
            this.sharedData = sharedData;
            this.hw3 = hw3;
        }

        public void actionPerformed(ActionEvent e) {
            System.out.println("inside and or action listener");
            JComboBox cb = (JComboBox) e.getSource();
            sharedData.setAndOrChoice((String) cb.getItemAt(cb.getSelectedIndex()));
        }

    }

    class CountryItemListener implements ItemListener {
        SharedData sharedData;
        hw3 hw3;

        public CountryItemListener(SharedData sharedData, final hw3 hw3) {
            this.sharedData = sharedData;
            this.hw3 = hw3;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            JCheckBox countryCb = (JCheckBox) e.getItem();
            String countryName = countryCb.getText();
            System.out.println(countryName);
            if (e.getStateChange() == ItemEvent.SELECTED) {
                sharedData.selectedCountry.add(countryName);
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                if (sharedData.selectedCountry.contains(countryName)) {
                    sharedData.selectedCountry.remove(countryName);
                }
            }
            
            try {
                if (sharedData.selectedGenre.isEmpty() || sharedData.getFromYear() == 0
                        || sharedData.getToYear() == 0) {
                    resetSelected();
                    resetList();
                    return;
                }

                sharedData.actors = new HashSet<>();
                sharedData.directors = new HashSet<>();
                sharedData.tagList = new HashSet<>();
                contentPane.revalidate();
                contentPane.repaint();

                queryActorFromDB();
                queryDirectorFromDB();
                queryTagsFromDB();

                displayActorPanel();
                displayDirPanel();
                displayTagPanel();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        }
    }

    class ActorComboBoxListener implements ItemListener {
        SharedData sharedData;

        public ActorComboBoxListener(SharedData sharedData) {
            this.sharedData = sharedData;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            String item = (String) e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                sharedData.selectedActors.add(item);
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                if (sharedData.selectedActors.contains(item)) {
                    sharedData.selectedActors.remove(item);
                }
            }
            
            try {
                sharedData.selectedDirector = null;
                sharedData.selectedTags = new HashSet<>();

                queryDirectorFromDB();
                queryTagsFromDB();
                displayDirPanel();
                displayTagPanel();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    class DirectorComboBoxListener implements ItemListener {
        SharedData sharedData;

        public DirectorComboBoxListener(SharedData sharedData) {
            this.sharedData = sharedData;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            String item = (String) e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                sharedData.selectedDirector = item;
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                if (sharedData.selectedDirector.equals(item)) {
                    sharedData.selectedDirector = null;
                }
            }

            try {
                if (sharedData.selectedGenre.isEmpty() || sharedData.getFromYear() == 0
                        || sharedData.getToYear() == 0) {
                    resetSelected();
                    resetList();
                    return;
                }

                sharedData.tagList = new HashSet<>();

                queryTagsFromDB();
                displayTagPanel();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void displayActorPanel() throws SQLException {
        actorPanel.removeAll();
        refreshGui();
        JComboBox<String> comboBoxActor_1 = new JComboBox<String>();
        comboBoxActor_1.setBounds(6, 22, 192, 27);
        actorPanel.add(comboBoxActor_1);

        JComboBox<String> comboBoxActor_2 = new JComboBox<String>();
        comboBoxActor_2.setBounds(6, 72, 192, 27);
        actorPanel.add(comboBoxActor_2);

        JComboBox<String> comboBoxActor_3 = new JComboBox<String>();
        comboBoxActor_3.setBounds(6, 122, 192, 27);
        actorPanel.add(comboBoxActor_3);

        if (!sharedData.actors.isEmpty()) {
            System.out.println("inside actor display");
            Iterator<String> actorItr = sharedData.actors.iterator();
            while (actorItr.hasNext()) {
                String actorName = actorItr.next();
                comboBoxActor_1.addItem(actorName);
                comboBoxActor_1.setSelectedIndex(-1);
                comboBoxActor_2.addItem(actorName);
                comboBoxActor_2.setSelectedIndex(-1);
                comboBoxActor_3.addItem(actorName);
                comboBoxActor_3.setSelectedIndex(-1);
            }
        }

        comboBoxActor_1.setSelectedIndex(-1);
        comboBoxActor_1.addItemListener(new ActorComboBoxListener(sharedData));
        comboBoxActor_2.setSelectedIndex(-1);
        comboBoxActor_2.addItemListener(new ActorComboBoxListener(sharedData));
        comboBoxActor_3.setSelectedIndex(-1);
        comboBoxActor_3.addItemListener(new ActorComboBoxListener(sharedData));

        refreshGui();
    }

    public void displayDirPanel() throws SQLException {
        dirPanel.removeAll();
        JComboBox comboBoxDir = new JComboBox();
        comboBoxDir.setBounds(6, 41, 192, 27);
        dirPanel.add(comboBoxDir);

        if (!sharedData.directors.isEmpty()) {
            Iterator<String> directorItr = sharedData.directors.iterator();
            while (directorItr.hasNext()) {
                String dirName = directorItr.next();
                comboBoxDir.addItem(dirName);
                comboBoxDir.setSelectedIndex(-1);
            }
        }
        
        comboBoxDir.setSelectedIndex(-1);
        comboBoxDir.addItemListener(new DirectorComboBoxListener(sharedData));

        refreshGui();
    }

    public void refreshGui() {
        contentPane.revalidate();
        contentPane.repaint();
        actorPanel.revalidate();
        actorPanel.repaint();
        countryPanel.revalidate();
        countryPanel.repaint();
        dirPanel.revalidate();
        dirPanel.repaint();
    }

    class GoActionListener implements ActionListener {
        SharedData sharedData;
        hw3 hw3;

        public GoActionListener(SharedData sharedData, final hw3 hw3) {
            this.sharedData = sharedData;
            this.hw3 = hw3;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton go = (JButton) e.getSource();
            if (hw3.fromTextField.getText().length() == 4) {
                sharedData.setFromYear(Integer.parseInt(hw3.fromTextField.getText()));
            }
            if (hw3.toTextField.getText().length() == 4) {
                sharedData.setToYear(Integer.parseInt(hw3.toTextField.getText()));
            }

            try {
                if (sharedData.selectedGenre.isEmpty() || sharedData.getFromYear() == 0
                        || sharedData.getToYear() == 0) {
                    resetSelected();
                    resetList();
                    return;
                }

                resetSelected();
                resetList();

                queryCountryFromDB();
                queryActorFromDB();
                queryDirectorFromDB();
                queryTagsFromDB();

                displayCountryPanel();
                displayActorPanel();
                displayDirPanel();
                displayTagPanel();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void displayCountryPanel() {
        countryPanel.removeAll();
        refreshGui();

        if (!sharedData.countries.isEmpty()) {
            List<JCheckBox> countryCheckBox = new ArrayList<JCheckBox>();

            int count = 0;
            Iterator<String> countryListItr = sharedData.countries.iterator();
            while (countryListItr.hasNext()) {
                String countryName = countryListItr.next();
                JCheckBox checkBox = new JCheckBox(countryName);

                countryCheckBox.add(count, checkBox);
                countryPanel.add(checkBox);
                checkBox.addItemListener(new CountryItemListener(sharedData, this));
                count++;
            }
        }
        countryPanel.setVisible(true);
        contentPane.revalidate();
        contentPane.repaint();
    }

    public void displayTagPanel() {
        if (sharedData.tagList.isEmpty()) {
            return;
        }
        List<JCheckBox> tagId = new ArrayList<>();
        Iterator<String> itr = sharedData.tagList.iterator();
        while (itr.hasNext()) {
            String tag = itr.next();
            JCheckBox tagCb = new JCheckBox(tag);
            tagId.add(tagCb);
            tagIdPanel.add(tagCb);
            tagCb.addItemListener(new TagItemListener(sharedData, this));
        }
        countryPanel.setVisible(true);
        contentPane.revalidate();
        contentPane.repaint();
    }

    
    
    public void displayTagWeight(){
        JComboBox<String> operators = new JComboBox<>();

        operators.setBounds(97, 18, 107, 27);
        tagWeightPanel.add(operators);
        operators.setSelectedItem(-1);
        operators.setModel(new DefaultComboBoxModel(new String[] { "=", ">", "<" }));
        operators.setToolTipText("compare\n");
        operators.addActionListener(new TagWeightActionListener(sharedData, this));
        
        JTextField value = new JTextField();
        value.setBounds(74, 57, 130, 33);
        tagWeightPanel.add(value);
        value.setColumns(10);
        
        JLabel lblTag = new JLabel("TagWeight:");
        lblTag.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblTag.setHorizontalAlignment(SwingConstants.CENTER);
        lblTag.setBounds(6, 18, 93, 23);
        tagWeightPanel.add(lblTag);
        
        JLabel lblValue = new JLabel("Value");
        lblValue.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
        lblValue.setBackground(Color.WHITE);
        lblValue.setHorizontalAlignment(SwingConstants.CENTER);
        lblValue.setBounds(15, 65, 61, 16);
        tagWeightPanel.add(lblValue);
        
        
    }
    
    class TagWeightActionListener implements ActionListener{
        SharedData sharedData;
        hw3 hw3;

        public TagWeightActionListener(SharedData sharedData, final hw3 hw3){
            this.sharedData = sharedData;
            this.hw3 = hw3;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("inside tagweight operator action listener");
            JComboBox cb = (JComboBox) e.getSource();
            sharedData.selectedTagOperator = (String) cb.getItemAt(cb.getSelectedIndex());
            
            
        }
        
    }
    
    private String generateFinalQuery() {
        if (sharedData.selectedGenre.isEmpty() || sharedData.getFromYear() == 0
                || sharedData.getToYear() == 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT MOVIEID, TITLE, YEAR, RTAUDIENCERATING, RTAUDIENCERATING FROM MOVIES_DATA WHERE MOVIEID IN (");
        sb.append(generateGenreYearQuery());
        String mid = movieIdFromSelectedCountries();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        mid = movieIdFromSelectedActors();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        mid = movieIdFromSelectedDirectors();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        mid = movieIdFromSelectedTags();
        if (mid != null) {
            sb.append(" INTERSECT ").append(mid);
        }
        sb.append(")");
        return sb.toString();
    }

    String movieIdFromSelectedTags() {
        if (sharedData.selectedTags.isEmpty()) {
            return null;
        }

        String tagList = "";
        Iterator<String> itr = sharedData.selectedTags.iterator();
        int count = 0;
        while (itr.hasNext() && count < sharedData.selectedTags.size() - 1) {
            String[] tagSplit = itr.next().split("\t");
            tagList = tagList + "'" + tagSplit[0] + "', ";
            count++;
        }
        String[] tagSplit = itr.next().split("\t");
        tagList = tagList + "'" + tagSplit[0] + "'";
        String mids = "SELECT MOVIEID FROM MOVIE_TAGS WHERE TAGID = ANY ( " + tagList + " )";
        return mids;
    }


    class TagItemListener implements ItemListener {
        SharedData sharedData;
        hw3 hw3;

        public TagItemListener(SharedData sharedData, final hw3 hw3) {
            this.sharedData = sharedData;
            this.hw3 = hw3;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            JCheckBox comboBox = (JCheckBox) e.getItem();
            String tagId = comboBox.getText();
            System.out.println(tagId);
            if (e.getStateChange() == ItemEvent.SELECTED) {
                sharedData.selectedTags.add(tagId);
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                if (sharedData.selectedTags.contains(tagId)) {
                    sharedData.selectedTags.remove(tagId);
                }
            }

            System.out.println(generateFinalQuery());
        }
    }
}

package com.shipra.imdb;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author shipra
 */
public class Populate {
    Connection conn;
    
    /**
     * InvalidEntryException class that handles any invalid data in any column
     * @author shipra
     *
     */
    class InvalidEntryException extends Exception{
        String data;
        public InvalidEntryException(String data){
                   this.data = data;
        }
    }
    
    public Populate() throws SQLException{
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return;
        }

        String host = "localhost";
        String dbName = "XE";
        int port = 49161;
        String oracleURL = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;

        String username = "scott";
        String password = "tiger";
        conn = DriverManager.getConnection(oracleURL, username, password);
        System.out.println("Conn success");
    }

    public void readDataMovies(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/movies.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/movies.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertMoviePrepStatement = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.trim().split("\t");
            int totalCols = cols.length;
            String insertMovieStatement = "INSERT INTO MOVIES_DATA "
                    + "(movieId, title, year, rtAudienceRating, rtAudienceNumRatings)"
                    + "VALUES (?, ?, ?, ?, ?)";
            insertMoviePrepStatement = conn.prepareStatement(insertMovieStatement);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1] + " " + data[5] + " " + data[17] + " " + data[18]);
                if (data.length == totalCols) {
                    
                    // movieId
                    try{
                        if(data[0].equals("\\N")){
                            throw new InvalidEntryException(data[0]);
                        }
                        insertMoviePrepStatement.setDouble(1, Double.parseDouble(data[0]));
                    } catch(InvalidEntryException e){
                        insertMoviePrepStatement.setDouble(1, 0);
                    }
                    
                    //title
                    try{
                        if(data[1].equals("\\N")){
                            throw new InvalidEntryException(data[1]);
                        }
                        insertMoviePrepStatement.setString(2, data[1]);
                    } catch(InvalidEntryException e){
                        insertMoviePrepStatement.setString(2, null);
                    }
                    
                    //year
                    try{
                        if(data[5].equals("\\N")){
                            throw new InvalidEntryException(data[5]);
                        }
                        insertMoviePrepStatement.setDouble(3, Double.parseDouble(data[5]));
                    } catch(InvalidEntryException e){
                        insertMoviePrepStatement.setDouble(3, 0);
                    }
                    
                    //rtAudienceRating
                    try{
                        if(data[17].equals("\\N")){
                            throw new InvalidEntryException(data[17]);
                        }
                        insertMoviePrepStatement.setDouble(4, Double.parseDouble(data[17]));
                    } catch(InvalidEntryException e){
                        insertMoviePrepStatement.setDouble(4, 0);
                    }
                    
                    
                    try{
                        if(data[18].equals("\\N")){
                            throw new InvalidEntryException(data[18]);
                        }
                        insertMoviePrepStatement.setDouble(5, Double.parseDouble(data[18]));
                    } catch(InvalidEntryException e){
                        insertMoviePrepStatement.setDouble(5, 0);
                    }
                    
                    insertMoviePrepStatement.executeUpdate();
//                    sb.append(data[0]).append("\t").append(data[1]).append("\t").append(data[5]).append("\t").append(data[17]).append("\t").append(data[18]).append("\n");
//                    bwG.write(sb.toString());
                } else {
                    bwB.write(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();
        insertMoviePrepStatement.close();
        conn.close();
    }


    public void readDataMovieGenre(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/movie_genres.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/movie_genres.dat.b");

        BufferedReader br = null;

        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertMovieGenrePrepSt = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertMovieGenreStatement = "INSERT INTO MOVIE_GENRE "
                    + "(movieId, genre)"
                    + "VALUES (?, ?)";
            insertMovieGenrePrepSt = conn.prepareStatement(insertMovieGenreStatement);  
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1]);
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\n");
                    bwG.write(sb.toString());
                    
                    insertMovieGenrePrepSt.setDouble(1, Double.parseDouble(data[0]));
                    insertMovieGenrePrepSt.setString(2, data[1]);
                    insertMovieGenrePrepSt.executeUpdate();
                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();

    }

    public void readDataMovieCountries(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/movie_countries.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/movie_countries.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertMovieCountryPrepSt = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertMovieCountry = "INSERT INTO MOVIE_COUNTRIES "
                    + "(MOVIEID, COUNTRY) "
                    + "VALUES (?, ?)";
            insertMovieCountryPrepSt = conn.prepareStatement(insertMovieCountry);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\n");
                    bwG.write(sb.toString());
                    
                    insertMovieCountryPrepSt.setDouble(1, Double.parseDouble(data[0]));
                    insertMovieCountryPrepSt.setString(2, data[1]);
                    insertMovieCountryPrepSt.executeUpdate();
                    
                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();
    }

    public void readDataMovieActors(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/movie_actors.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/movie_actors.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertMovieActorPrepSt = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertDataMovieActor = "INSERT INTO MOVIE_ACTORS "
                    + "(MOVIEID, ACTORID, ACTORNAME, RANKING) "
                    + "VALUES (?, ?, ?, ?)";
            insertMovieActorPrepSt = conn.prepareStatement(insertDataMovieActor);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1] + " " + data[2] + " " + data[3]);
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\t").append(data[2]).append("\t").append(data[3]).append("\n");
                    bwG.write(sb.toString());
                    insertMovieActorPrepSt.setInt(1, Integer.parseInt(data[0]));
                    insertMovieActorPrepSt.setString(2, data[1]);
                    insertMovieActorPrepSt.setString(3, data[2]);
                    insertMovieActorPrepSt.setInt(4, Integer.parseInt(data[3]));
                    insertMovieActorPrepSt.executeUpdate();

                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();

    }

    public void readDataUserTaggedMovies(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/user_taggedmovies.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/user_taggedmovies.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertUserTagMoviePrepSt = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertDataUserTagMovie = "INSERT INTO USER_TAGGED_MOVIES "
                    + "(USERID, MOVIEID, TAGID) "
                    + "VALUES (?, ?, ?)";
            insertUserTagMoviePrepSt = conn.prepareStatement(insertDataUserTagMovie);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1] + " " + data[2]);
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\t").append(data[2]).append("\n");
                    bwG.write(sb.toString());
                    insertUserTagMoviePrepSt.setDouble(1, Double.parseDouble(data[0]));
                    insertUserTagMoviePrepSt.setDouble(2, Double.parseDouble(data[1]));
                    insertUserTagMoviePrepSt.setDouble(3, Double.parseDouble(data[2]));  
                    insertUserTagMoviePrepSt.executeUpdate();
                    
                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();

    }

    public void readDataMovieDirectors(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/movie_directors.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/movie_directors.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertMovieDir = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertDataMovieDir = "INSERT INTO MOVIE_DIRECTORS "
                    + "(MOVIEID, DIRECTORID, DIRECTORNAME) "
                    + "VALUES (?, ?, ?)";
            insertMovieDir = conn.prepareStatement(insertDataMovieDir);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1] + " " + data[2]);
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\t").append(data[2]).append("\n");
                    bwG.write(sb.toString());
                    insertMovieDir.setDouble(1, Double.parseDouble(data[0]));
                    insertMovieDir.setString(2, data[1]);
                    insertMovieDir.setString(3,data[2]);
                    insertMovieDir.executeUpdate();
                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();

    }

    public void readDataMovieTag(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/movie_tags.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/movie_tags.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertMovieTag = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertDataMovieTag = "INSERT INTO MOVIE_TAGS "
                    + "(MOVIEID, TAGID, TAGWEIGHT) "
                    + "VALUES (?, ?, ?)";
            insertMovieTag = conn.prepareStatement(insertDataMovieTag);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1] + " " + data[2]);
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\n");
                    bwG.write(sb.toString());
                    insertMovieTag.setDouble(1, Double.parseDouble(data[0]));
                    insertMovieTag.setDouble(2, Double.parseDouble(data[1]));
                    insertMovieTag.setDouble(3, Double.parseDouble(data[2]));
                    insertMovieTag.executeUpdate();
                    
                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();

    }

    public void readDataTags(String inputFileName) throws IOException, SQLException {
        String moviesDataFile = inputFileName;
        File moviesDataGood = new File("/Users/shipra/Movies/imdbFiles/tags.dat.g");
        File moviesDataBad = new File("/Users/shipra/Movies/imdbFiles/tags.dat.b");

        BufferedReader br = null;
        BufferedWriter bwG = null;
        BufferedWriter bwB = null;
        PreparedStatement insertTags = null;
        try {
            bwG = new BufferedWriter(new FileWriter(moviesDataGood));
            bwB = new BufferedWriter(new FileWriter(moviesDataBad));

            br = new BufferedReader(new FileReader(moviesDataFile));
            String line = null;
            line = br.readLine();
            String[] cols = line.split("\t");
            int totalCols = cols.length;
            String insertDataTags = "INSERT INTO TAGS "
                    + "(ID, VALUE) "
                    + "VALUES (?, ?)";
            insertTags = conn.prepareStatement(insertDataTags);
            while ((line = br.readLine()) != null) {
                StringBuilder sb = new StringBuilder();
                String[] data = line.split("\t");
                System.out.println(data[0] + " " + data[1]);
                if (data.length == totalCols) {
                    sb.append(data[0]).append("\t").append(data[1]).append("\n");
                    bwG.write(sb.toString());
                    insertTags.setDouble(1, Double.parseDouble(data[0]));
                    insertTags.setString(2, data[1]);
                    insertTags.executeUpdate();
                } else {
                    bwB.write(line);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        bwG.close();
        bwB.close();

    }

    public static void main(String[] args) throws IOException, SQLException {
        Populate populateObj = new Populate();
        
       // populateObj.readDataMovies(args[0]);
        //populateObj.readDataTags(args[1]);
        //populateObj.readDataUserTaggedMovies(args[2]);

       // populateObj.readDataMovieGenre(args[3]);
      //  populateObj.readDataMovieCountries(args[4]);
        populateObj.readDataMovieTag(args[5]);

      // populateObj.readDataMovieActors(args[6]);
       // populateObj.readDataMovieDirectors(args[7]);
    }

}

package com.shipra.imdb;

import java.util.HashSet;
import java.util.Set;

public class SharedData {
    Set<String> countries;
    Set<String> actors;
    Set<String> directors;
    Set<String> tagList;

    double fromYear;
    double toYear;

    String andOrChoice;
    String selectedTagOperator;
    String selectedTagValue;
    Set<String> selectedGenre;
    Set<String> selectedCountry;
    Set<String> selectedActors;
    // Default value is null
    String selectedDirector;
    Set<String> selectedTags;

    public SharedData() {
        countries = new HashSet<String>();
        actors = new HashSet<String>();
        directors = new HashSet<>();
        tagList = new HashSet<>();
        selectedTagOperator = "=";
        andOrChoice = "OR";

        selectedGenre = new HashSet<>();
        selectedCountry = new HashSet<>();
        selectedActors = new HashSet<>();
        selectedTags = new HashSet<>();
    }

    public String getAndOrChoice() {
        return andOrChoice;
    }

    public void setAndOrChoice(String andOrChoice) {
        this.andOrChoice = andOrChoice;
    }

    public double getFromYear() {
        return fromYear;
    }

    public void setFromYear(double fromYear) {
        this.fromYear = fromYear;
    }

    public double getToYear() {
        return toYear;
    }

    public void setToYear(double toYear) {
        this.toYear = toYear;
    }
}
